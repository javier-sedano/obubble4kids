package com.Odroid.Obubble4kids;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

public class Bubble implements Parcelable {
  private static final float DENSITY = 0.05f; // It only matters as a relation to Ball.MASS.
  public float x;
  public float y;
  public float vx;
  public float vy;
  public float r;
  public float m;
  public float area;

  private Bitmap _bitmap = null;
  
  public Bubble(float x, float y, float vx, float vy, float r) {
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
    this.r = r;
    this.m = _mass(r);
    this.area = _area(r);
  }
  
  // Just for debugging
  public Bubble(int maxx, int maxy, float maxv, float maxr) {
    this.x = (float)Math.random()*maxx;
    this.y = (float)Math.random()*maxy;
    this.vx = (float)Math.random()*maxv*2-maxv;
    this.vy = (float)Math.random()*maxv*2-maxv;
    this.r = (float)Math.random()*maxr;
    if (this.r < (maxr/10)) this.r = maxr/10;
    this.m = _mass(r);
    this.area = _area(r);
  }

  public Bubble(Parcel p) {
    float[] values = new float[5];
    p.readFloatArray(values);
    this.x = values[0];
    this.y = values[1];
    this.vx = values[2];
    this.vy = values[3];
    this.r = values[4];
    this.m = _mass(r);
    this.area = _area(r);
  }

  public void dump(String prefix) {
    android.util.Log.d("Ball", prefix+x+","+y+" "+vx+","+vy+" "+r+":"+m);
  }

  private float _mass(float r) {
    return (float) r * DENSITY;
    // It should be:
    // return (float) r * r * r * DENSITY; // PI and 4/3 an be forgotten, just adjust DENSITY
    // But that's too much difference between small and large bubbles  
  }
  
  private float _area(float r) {
    return (float) Math.PI * r * r;
  }
  
  public int describeContents() {
    return 0;
  }

  public void writeToParcel(Parcel p, int flags) {
    float[] values = new float[5];
    values[0] = x;
    values[1] = y;
    values[2] = vx;
    values[3] = vy;
    values[4] = r;
    p.writeFloatArray(values);
  }

  public static final Parcelable.Creator<Bubble> CREATOR = new Parcelable.Creator<Bubble>() {
    public Bubble createFromParcel(Parcel in) {
      return new Bubble(in);
    }
    public Bubble[] newArray(int size) {
      return new Bubble[size];
    }
  };

}
