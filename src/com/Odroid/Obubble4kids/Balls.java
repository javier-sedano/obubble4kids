package com.Odroid.Obubble4kids;

import java.util.Vector;

import android.os.Parcelable;

public class Balls extends Vector<Ball> {
  
  public Balls(int number, int maxx, int maxy, float maxv) {
//    add(new Ball(10f, 100f, 0.05f, 0f));
//    add(new Ball(100f, 100f, -0.01f, 0f));
//  add(new Ball(50f, 50f, 0f, 0.05f));
//  add(new Ball(50f, 100f, 0f, 0.01f));
//  add(new Ball(50f, 100f, 0.05f, 0f));
//  add(new Ball(100f, 99f, 0f, 0f));
    for (int i=0; i<number; i++) {
      Ball b = new Ball(maxx, maxy, maxv);
      add(b);
    }
  }
  
  public Balls(Parcelable[] balls) {
    for (Parcelable b:balls) {
      if (b instanceof Ball) {
        add((Ball)b);
      }
    }
  }
}
