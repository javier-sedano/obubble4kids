package com.Odroid.Obubble4kids;

import android.os.Parcel;
import android.os.Parcelable;

public class Ball implements Parcelable {
  public float x = 0;
  public float y = 0;
  public float vx = 0;
  public float vy = 0;
  public static final float MASS = 1;

  public Ball(float x, float y, float vx, float vy) {
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
  }
  
  public Ball(int maxx, int maxy, float maxv) {
    this.x = (float)Math.random()*maxx;
    this.y = (float)Math.random()*maxy;
    while (Math.abs(this.vx) < (maxv/3)) {
      this.vx = (float)Math.random()*maxv*2-maxv;
    }
    while (Math.abs(this.vy) < (maxv/3)) {
      this.vy = (float)Math.random()*maxv*2-maxv;
    }
  }
  
  public Ball(Parcel p) {
    float[] values = new float[4];
    p.readFloatArray(values);
    this.x = values[0];
    this.y = values[1];
    this.vx = values[2];
    this.vy = values[3];
  }
  
  public void dump(String prefix) {
    android.util.Log.d("Ball", prefix+x+","+y+" "+vx+","+vy);
  }

  public int describeContents() {
    return 0;
  }

  public void writeToParcel(Parcel p, int flags) {
    float[] values = new float[4];
    values[0] = x;
    values[1] = y;
    values[2] = vx;
    values[3] = vy;
    p.writeFloatArray(values);
  }

  public static final Parcelable.Creator<Ball> CREATOR = new Parcelable.Creator<Ball>() {
    public Ball createFromParcel(Parcel in) {
      return new Ball(in);
    }
    public Ball[] newArray(int size) {
      return new Ball[size];
    }
  };
}
