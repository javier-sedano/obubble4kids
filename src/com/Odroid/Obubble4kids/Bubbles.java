package com.Odroid.Obubble4kids;

import java.util.Vector;

import android.os.Parcelable;

public class Bubbles extends Vector<Bubble> {

  public Bubbles() {
    super();
  }
  
  // Just for debugging
  public Bubbles(int number, int maxx, int maxy, float maxv, float maxr) {
//    add(new Bubble(10f, 90f, 0.1f, 0, 10));
//    add(new Bubble(160f, 80f, 0, 0, 20));
//    add(new Bubble(160f, 60f, 0, 0, 40));
//    add(new Bubble(160f, 20f, 0, 0, 80));
//    add(new Bubble(10f, 95f, 0.1f, 0, 10));
//    add(new Bubble(10f, 140f, 0.1f, 0, 20));
//    add(new Bubble(10f, 30f, 0.1f, 0, 40));
//    add(new Bubble(100f, 95f, 0, 0, 10));
//    add(new Bubble(100f, 65f, 0, 0, 40));
    for (int i=0; i<number; i++) {
      Bubble b = new Bubble(maxx, maxy, maxv, maxr);
      add(b);
    }
  }

  public Bubbles(Parcelable[] bubbles) {
    for (Parcelable b:bubbles) {
      if (b instanceof Bubble) {
        add((Bubble)b);
      }
    }
  }
}
